package com.asurplus.service;

import com.asurplus.common.enums.BackupCategoryEnum;
import com.asurplus.common.vo.BackupJobVO;
import com.asurplus.entity.BackupLog;
import com.asurplus.common.vo.LayuiTablePojo;
import com.asurplus.common.utils.RES;
import com.asurplus.common.vo.BackupLogVO;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @since 2022-08-05
 */
public interface BackupLogService extends IService<BackupLog> {

    LayuiTablePojo list(Integer page, Integer limit, BackupLogVO backUpLogVO);

    BackupLog saveBackupLog(BackupJobVO jobVO, BackupCategoryEnum categoryEnum);

    BackupLogVO getBackUpLogVO(Integer id);

    RES delete(Integer id);

    RES deleteAll();


}
