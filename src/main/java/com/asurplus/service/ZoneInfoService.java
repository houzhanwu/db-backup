package com.asurplus.service;

import com.asurplus.entity.ZoneInfo;
import com.asurplus.common.vo.LayuiTablePojo;
import com.asurplus.common.utils.RES;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @since 2022-08-05
 */
public interface ZoneInfoService extends IService<ZoneInfo> {

    LayuiTablePojo list(Integer page, Integer limit, ZoneInfo zoneInfo);

    RES add(ZoneInfo zoneInfo);

    RES update(ZoneInfo zoneInfo);

    RES delete(Integer id);

    List<ZoneInfo> listSelect();
}
