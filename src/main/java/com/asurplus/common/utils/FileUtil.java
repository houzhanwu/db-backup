package com.asurplus.common.utils;

import lombok.extern.slf4j.Slf4j;
import org.apache.http.entity.ContentType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

/**
 * 文件处理工具类
 *
 * @author asurplus
 */
@Slf4j
public class FileUtil {

    /**
     * File 转换为 MultipartFile
     *
     * @param file
     * @return
     */
    public static MultipartFile file2MultipartFile(File file) {
        try {
            FileInputStream fileInputStream = new FileInputStream(file);
            return new MockMultipartFile(file.getName(), file.getName(), ContentType.APPLICATION_OCTET_STREAM.toString(), fileInputStream);
        } catch (IOException e) {
            log.error("File to MultipartFile 异常：{}", e.getMessage());
        }
        return null;
    }
}
