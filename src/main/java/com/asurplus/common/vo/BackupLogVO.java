package com.asurplus.common.vo;

import com.asurplus.entity.BackupLog;
import lombok.Data;

@Data
public class BackupLogVO extends BackupLog {

    private Integer zoneId;

    private String zoneName;

    private String nodeName;

    private String nodeText;

    private String timeRange;
}
