package com.asurplus.common.vo;

import com.asurplus.entity.BackupTask;
import lombok.Data;

@Data
public class BackupTaskVO extends BackupTask {

    private Integer zoneId;

    private String zoneName;

    private String nodeName;
}
