package com.asurplus.common.vo;

import lombok.Data;

@Data
public class BackupTaskUpdateReqVO {

    private Integer id;

    private String cron;

    private Boolean status;

    private String remark;

    /**
     * 是否压缩
     */
    private Boolean isCompress = false;
    /**
     * 是否上传
     */
    private Boolean isUpload = false;
    /**
     * 是否删除
     */
    private Boolean isDelete = false;
}
