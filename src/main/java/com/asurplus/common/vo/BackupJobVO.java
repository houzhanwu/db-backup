package com.asurplus.common.vo;

import lombok.Data;

@Data
public class BackupJobVO {

    private Integer nodeId;

    private String host;

    private String port;

    private String username;

    private String password;

    private String database;

    private String[] tables;

    /**
     * 备份形式
     * 0-只备份表结构
     * 1-只备份表数据
     * 2-备份表结构+表数据
     */
    private Integer dataType;

    /**
     * 是否压缩
     */
    private Boolean isCompress = false;
    /**
     * 是否上传
     */
    private Boolean isUpload = false;
    /**
     * 是否删除
     */
    private Boolean isDelete = false;
}
