package com.asurplus.common.vo;

import lombok.Data;

import java.io.File;

/**
 * 备份响应数据
 */
@Data
public class BackupRespVO {

    private String msg;

    private File file;

    public boolean isSuccess() {
        return null != this.file && 0 < this.file.length();
    }
}
