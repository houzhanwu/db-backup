package com.asurplus.common.vo;

import com.baomidou.mybatisplus.core.metadata.IPage;
import lombok.Data;

import java.util.List;

/**
 * 后台返回给LayUI表格的数据格式
 */
@Data
public class LayuiTablePojo {

    /**
     * 接口状态
     */
    private Integer code;

    /**
     * 提示信息
     */
    private String msg;

    /**
     * 接口数据长度
     */
    private Long count;

    /**
     * 接口数据
     */
    private List<?> data;

    private static LayuiTablePojo resultData(Integer code, String msg, Long count, List<?> data) {
        LayuiTablePojo res = new LayuiTablePojo();
        res.setCode(code);
        res.setMsg(msg);
        res.setCount(count);
        res.setData(data);
        return res;
    }

    /**
     * 返回数据给表格
     */
    public static LayuiTablePojo ok(IPage iPage) {
        return resultData(0, "查询成功", iPage.getTotal(), iPage.getRecords());
    }

    /**
     * 返回数据给表格
     */
    public static LayuiTablePojo ok(Long count, List<?> data) {
        return resultData(0, "查询成功", count, data);
    }

    /**
     * 返回数据给表格
     */
    public static LayuiTablePojo ok(Integer count, List<?> data) {
        return resultData(0, "查询成功", count.longValue(), data);
    }

    /**
     * 返回数据给表格
     */
    public static LayuiTablePojo no() {
        return resultData(1, "查询失败", 0L, null);
    }
}
