package com.asurplus.common.vo;

import com.asurplus.common.enums.BackupCategoryEnum;
import lombok.Data;

@Data
public class BackupReqVO {

    private Integer nodeId;

    private String database;

    private String[] tables;

    /**
     * 备份形式
     * 0-只备份表结构
     * 1-只备份表数据
     * null-备份表结构+表数据
     */
    private Integer dataType;

    private BackupCategoryEnum categoryEnum;

    private String cron;

    private Boolean status;

    private String remark;

    /**
     * 是否压缩
     */
    private Boolean isCompress = false;
    /**
     * 是否上传
     */
    private Boolean isUpload = false;
    /**
     * 是否删除
     */
    private Boolean isDelete = false;
}
