package com.asurplus.mapper;

import com.asurplus.entity.ZoneInfo;
import com.asurplus.common.vo.ZoneInfoVO;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @since 2022-08-05
 */
public interface ZoneInfoMapper extends BaseMapper<ZoneInfo> {

    Page<ZoneInfoVO> list(Page<ZoneInfoVO> page, @Param(Constants.WRAPPER) Wrapper<ZoneInfoVO> queryWrapper);
}
