package com.asurplus.mapper;

import com.asurplus.entity.NodeInfo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @since 2022-08-04
 */
public interface NodeInfoMapper extends BaseMapper<NodeInfo> {

}
